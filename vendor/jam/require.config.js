var jam = {
    packages: [{
        name: "backbone",
        location: "http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js"
    }, {
        name: "backbone.layoutmanager",
        location: "/vendor/jam/backbone.layoutmanager",
        main: "backbone.layoutmanager.js"
    }, {
        name: "jquery",
        location: "http://code.jquery.com/jquery-1.9.1.min.js"
    }, {
        name: "lodash",
        location: "http://cdnjs.cloudflare.com/ajax/libs/lodash.js/1.0.1/lodash.min.js"
    }, {
        name: "underscore",
        location: "/vendor/jam/underscore",
        main: "underscore.js"
    }],
    version: "0.2.17",
    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "backbone.layoutmanager": {
            deps: ["jquery", "backbone", "underscore"],
            exports: "Backbone.Layout"
        },
        underscore: {
            exports: "_"
        }
    }
};
if (typeof require != "undefined" && require.config) require.config({
    packages: [{
        name: "backbone",
        location: "/vendor/jam/backbone",
        main: "backbone.js"
    }, {
        name: "backbone.layoutmanager",
        location: "/vendor/jam/backbone.layoutmanager",
        main: "backbone.layoutmanager.js"
    }, {
        name: "jquery",
        location: "/vendor/jam/jquery",
        main: "dist/jquery.js"
    }, {
        name: "lodash",
        location: "/vendor/jam/lodash",
        main: "./lodash.js"
    }, {
        name: "underscore",
        location: "/vendor/jam/underscore",
        main: "underscore.js"
    }],
    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "backbone.layoutmanager": {
            deps: ["jquery", "backbone", "underscore"],
            exports: "Backbone.Layout"
        },
        underscore: {
            exports: "_"
        }
    }
});
else var require = {
    packages: [{
        name: "backbone",
        location: "/vendor/jam/backbone",
        main: "backbone.js"
    }, {
        name: "backbone.layoutmanager",
        location: "/vendor/jam/backbone.layoutmanager",
        main: "backbone.layoutmanager.js"
    }, {
        name: "jquery",
        location: "/vendor/jam/jquery",
        main: "dist/jquery.js"
    }, {
        name: "lodash",
        location: "/vendor/jam/lodash",
        main: "./lodash.js"
    }, {
        name: "underscore",
        location: "/vendor/jam/underscore",
        main: "underscore.js"
    }],
    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "backbone.layoutmanager": {
            deps: ["jquery", "backbone", "underscore"],
            exports: "Backbone.Layout"
        },
        underscore: {
            exports: "_"
        }
    }
};
typeof exports != "undefined" && typeof module != "undefined" && (module.exports = jam);
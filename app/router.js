define([
        "app", 
        "vent",
        "modules/side_list",
        "modules/slideshow"
        ], function (App, vent, SideList, Slideshow) {
    var Router = Backbone.Router.extend({
        className: "fullSlider",
        routes: {
            "": "index",
        },
        index: function () {
            // Creating  the inital layout
            var main = new Backbone.Layout({
                template: "side_list/layout",
                // Attach these views to these classes
                views: {
                    ".faculty-list": new SideList.Views.ListView(),
                    ".faculty-slideshow": new Slideshow.Views.Default()
                },
            });
            // Empty #main, and add the new view in.
            $("#main").empty().append(main.el),
            // Render everything
            main.render();
        },
    });
    return Router;
});

// Set the require.js configuration for your application.
require.config({
    deps: ["/vendor/jam/require.config.js", "/app/main.js"],
    paths: {
        lodash: "/vendor/jam/lodash/dist/lodash.underscore",
    },
    map: {
        "*": {
            underscore: "lodash"
        }
    },
    shim: {
    }
});
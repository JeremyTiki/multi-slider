define(["app"], function(app) {
    var EventAgr = {};
    // Creates an event agregrator for Publisher/Consumer model
    _.extend(EventAgr, Backbone.Events);

    return EventAgr;
});

define(["backbone.layoutmanager"], function (e) {
    var t = {
        root: "/"
    }, n = window.JST = window.JST || {};
    return e.configure({
        manage: !0,
        prefix: "app/templates/",
        fetch: function (e) {
            e += ".html";
            if (n[e]) return n[e];
            var r = this.async();
            $.get(t.root + e, function (e) {
                r(_.template(e))
            }, "text")
        }
    }), _.extend(t, {
        module: function (e) {
            return _.extend({
                Views: {}
            }, e)
        },
        useLayout: function (e, t) {
            return _.isObject(e) && (t = e), t = t || {}, _.isString(e) && (t.template = e), this.layout ? this.layout.template = t.template : this.layout = new Backbone.Layout(_.extend({
                el: "main"
            }, t)), this.layout
        }
    }, Backbone.Events)
});
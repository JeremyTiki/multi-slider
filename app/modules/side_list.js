define(["app", "vent"],function(app, vent){

    var SideList = app.module()
    
    SideList.Model = Backbone.Model.extend({
        defaults: {
            "id": "000",
            "name": "Default Name",
            "position": "Default Position",
            "image": "/img/default.png"
        },
        // Normally models create a unique id, like cid001, but this forces all
        // models to use the passed in {id}.
        idAttribute: "id",
    });

    SideList.Collection = Backbone.Collection.extend({
        model: SideList.Model,
        url: "/data/data.json"
    });

    SideList.Views.ListView = Backbone.View.extend({
        manage: true,
        tagName: "ul",
        events: {
            "click .facultyMember": "buildSlideshow",
        },
        initialize: function () {
            var that = this;
            this.collection = new SideList.Collection();
            // Forces collection to trigger the reset event
            this.collection.fetch({reset: true});
        },
        beforeRender: function() {
            // Iterate through the collection and attach an ItemView for each
            this.collection.each(function(model) {
                this.insertView(new SideList.Views.ItemView({
                    serialize: model.toJSON(),
                    id: model.get('id'),
                }));
            }, this);
        },
        /*
         * Name buildSlideshow
         * Accepts: event trigger
         * Returns: void
         * Description: This view is triggered by clicking on a .facultyMember.
         * The main point of this view is to add/remove the class active from
         * the faculty list. In addition this causes the vent event agregrator
         * to trigger "buildSlideshow" which runs in Slideshow.Views.Default
         */
        buildSlideshow: function (e) {
            var current_target = $(e.currentTarget),
                info = current_target.attr("id"),
                gallery = this.collection.get(info).attributes.gallery,
                passing = {info: info, gallery: gallery}
            vent.trigger("buildSlideshow", passing);
            $("li.facultyMember").each( function () {
                $(this).removeClass("active");
            });
            current_target.addClass("active");
        }
    })

    SideList.Views.ItemView = Backbone.View.extend({
        manage: true,
        tagName: "li",
        className: "facultyMember",
        template: "side_list/list",
    });

    return SideList;
});

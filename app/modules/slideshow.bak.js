        afterRender: function () {
            this.$el.append("<li class='arrow-right'></li>");
            this.index = this.collection.at(0).get("id");
            $("#image_" + this.index).addClass("active");
            this.index =  parseInt(this.index)+1;
            this.last =  this.collection.at(0).get("id");
            this.original = this.collection.at(0).get("id");
            this.original =  parseInt(this.original);
        },
        moveForward: function () {
            if($("#image_" + this.index).length == 1) {
                $("#image_" + this.last).removeClass("active");
                $("#image_" + this.index).addClass("active");
                this.index++;
                this.last++;
            } else {
                console.log(this.index);
                console.log(this.last);
                $("#image_" + this.last).removeClass("active");
                this.index = this.original;
                $("#image_" + this.original).addClass("active");
                this.last = this.index;
                this.index++;
            }
        }
    });

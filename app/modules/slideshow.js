define(["app", "vent"], function(app, vent) {

    var Slideshow = app.module();
   
    Slideshow.Model = Backbone.Model.extend({
        defaults: {
            id: "000",
            src: "/img/photo_deafult.png",
            type: "image"
        },
        sync: function () { return false; }

    });

    Slideshow.Collection = Backbone.Collection.extend({
        model: Slideshow.Model,
        url: "/data/data.json"
    });

    Slideshow.Views.Default = Backbone.View.extend({
        className: "defaultSlideshow",
        template: "slideshow/slideshow_layout",
        initialize: function () {
            // Binds context to buildSlideshow function
            _.bindAll(this, "buildSlideshow");
            // Listens for an event "buildSlideshow" and then triggers
            // this.buildSlideshow
            vent.bind("buildSlideshow", this.buildSlideshow);
            this.slideLeftHide();
        },
        /*
         * Name: buildSlideshow
         * Accepts: Object({gallery: [Object()], info: model_id})
         * Notes on Accepts: Object.gallery needs to be a list of objects that
         *                   mirrors Slideshow.Model
         * Returns: void
         * Description: This event is never triggered by this view, it is
         * instead triggered by the event agregrator.
         *
         */
        buildSlideshow: function (passing) {
            this.slideLeftHide();
            // Context binding
            var that = this;
            // Create new collection
            this.collection = new Slideshow.Collection();
            this.faculty_id = passing.info; 
            // iterates over passed in options (should be a list of objects)
            // and creates a new collection
            _.each(passing.gallery,  function(model) {
                that.collection.create(model);
            });
            // Sets a new view on .slideshow-items, and passes the collection
            // for items
            this.setView(".slideshow-items", new Slideshow.Views.SlideListView({
                collection: this.collection.toJSON()
            }));
            this.setView(".slideshow-thumbs", new Slideshow.Views.ThumbListView({
                collection: this.collection.toJSON()
            }))
            this.render();
            this.slideLeftShow();
        },
        slideLeftHide: function () {
            this.$el.animate({
                width: '0px'
            }, 400);
        },
        slideLeftShow: function () {
            this.$el.animate({
                width: '600px'
            }, 400);
        },
        /**
         * Name: afterRender
         * Accepts: None
         * Returns: void
         * Description: Runs after render each time, this allows the first item
         * to show up
         **/
        afterRender: function () {
            $(".slideshow-items ul li:nth-child(2)").show();
        }
    });

    Slideshow.Views.SlideListView = Backbone.View.extend({
        events: {
            "click .arrow-right": "moveForward",
            "click .arrow-left": "moveBackward",
        },
        tagName: "ul",
        initialize: function (options) { 
            // Might need this later
            this.temp = this.options.collection;
            // Add .arrow-left to the element before render
            this.$el.append("<li class='arrow-left'></li>");
            // this.prefix is used later in program
            this.prefix = "#item_";
            _.bindAll(this, "clickedThumb");
            vent.bind("clickedThumb", this.clickedThumb);
        },
        beforeRender: function () {
            // Keep context
            var that = this;
            // Build collection from bassed in objects
            this.collection = new Slideshow.Collection();
            _.each(this.temp,  function(model) {
                that.collection.create(model);
            });

            // for each item in the collection, createa  new view
            // with an id from the model
            var counter = 0;
            this.collection.each(function(model) {
                if(model.get('type') == "image") {
                    this.insertView(new Slideshow.Views.SlideImageItemView({
                        serialize: model.toJSON(),
                        id: "item_" + counter,
                    }));
                } else if (model.get('type') == "video") {
                    this.insertView(new Slideshow.Views.SlideVideoItemView({
                        serialize: model.toJSON(),
                        id: "item_" + counter,
                    }));
                } else {
                    console.log("No support for type " + model.get('type'));
                }
                counter++;
            }, this);
        },
        /*
         * Name: afterRender
         * Accepts: None
         * Returns: void
         * Description: This function is called after the object is rendered
         */
        afterRender: function () {
            // After render add the right arrow
            this.$el.append("<li class='arrow-right'></li>");
            // Set the index to the first model id
            // The index keeps track of of what item is currently showing
            this.index = 0;
            $("#item_" + this.index).fadeIn(1000);
            // Last is used to mark the previous item
            this.last = -1;
            // Original is used to keep track of the first item in the
            // slideshow
            this.original = 0
            // End is used to keep track of the final item in the slideshow
            this.end = $(".slide").length - 1;
        },
        /*
         * Name: moveForward
         * Accepts: None
         * Returns: void
         * Description: This function is called when a user clicks on
         * .left-button
         */
        moveForward: function () {
            // If the index id is equal to the end item id
            if(this.index == this.end) {
                // Set index to original, essentially reseting the slideshow
                this.index = this.original;
                this.last = this.index - 1;
                $(this.prefix + this.end).fadeOut(1000)
                $(this.prefix + this.original).fadeIn(1000);
            // If index is not equal to end
            } else {
                // Forward variable is equal to the next item
                var forward = this.index + 1;
                $(this.prefix + this.index).fadeOut(1000);
                $(this.prefix + forward).fadeIn(1000);
                this.index++;
                this.last++;
            }
            vent.trigger("moveForwardThumb", this.index);
        },
        /*
         * Name: moveBackward
         * Accepts: None
         * Returns: void
         * Description: This function is called when a user clicks on
         * .right-button
         */
        moveBackward: function () {
            if(this.index == this.original) {
                this.index = this.end;
                this.last = this.index - 1;
                $(this.prefix + this.original).fadeOut(1000);
                $(this.prefix + this.end).fadeIn(1000);
            } else {
                var backward = this.index - 1;
                $(this.prefix + this.index).fadeOut(1000);
                $(this.prefix + backward).fadeIn(1000);
                this.index--;
                this.last--;
            }
            vent.trigger("moveBackwardThumb", this.index);
        },
        /**
         * Name: clickedThumb
         * Accepts: EventAgregrator int
         * Returns: void
         * Description: Clicking .slide triggers this event via the Event
         * Agregrator. This function fadesOut the current slide, updates, and
         * fadesIn the new slide; as well as reseting the current this.index
         * and this.last
         **/
        clickedThumb: function (current_id) {
            $(this.prefix + this.index).fadeOut(1000);
            this.index = current_id.substring(6);
            $(this.prefix + this.index).fadeIn(1000);
            console.log(this.prefix + this.index);
            this.last = -1;
 
        },
    });
    /*
     * Name: Slideshow.Views.SlideImageItemView
     * Description: Represents a single full image
     */
    Slideshow.Views.SlideImageItemView = Backbone.View.extend({
        template: "slideshow/slideshow_item_image",
        tagName: "li",
        className: "image_slideshow slide"
    });
    Slideshow.Views.SlideVideoItemView = Backbone.View.extend({
        template: "slideshow/slideshow_item_video",
        tagName: "li",
        className: "video_slideshow slide"
    });
    
    Slideshow.Views.ThumbListView = Backbone.View.extend({
        events: {
            "click li.thumb_nail": "selectThumb"
        },
        tagName: "ul",
        initialize: function (options) { 
            _.bindAll(this, "moveBackwardThumb", "moveForwardThumb")
            vent.bind("moveBackwardThumb", this.moveBackwardThumb);
            vent.bind("moveForwardThumb", this.moveForwardThumb);
            this.image_template = "slideshow/thumbs_image_item";
            this.video_template = "slideshow/thumbs_video_item";
            // Might need this later
            this.temp = this.options.collection;
            
        },
        beforeRender: function () {
            // Keep context
            var that = this;
            // Build collection from bassed in objects
            this.collection = new Slideshow.Collection();
            _.each(this.temp,  function(model) {
                that.collection.create(model);
            });

            // for each item in the collection, createa  new view
            // with an id from the model
            var counter = 0;
            this.collection.each(function(model) {
                if(model.get('type') == "image") {
                    this.insertView(new Slideshow.Views.ThumbItemView({
                        serialize: model.toJSON(),
                        id: "thumb_" + counter,
                        template: this.image_template,
                    }));
                } else if (model.get('type') == "video") {
                    this.insertView(new Slideshow.Views.ThumbItemView({
                        serialize: model.toJSON(),
                        id: "thumb_" + counter,
                        template: this.video_template,
                    }));

                } else {
                    console.log("No support for type " + model.get('type'));
                }
                counter++;
            }, this);
        },
        afterRender: function () {
            $("#thumb_0").addClass("selected");
        },
        moveForwardThumb: function (index) {
            this.removeClass();
            $("#thumb_" + index).addClass("selected");
        },
        moveBackwardThumb: function (index) {
            $(".slideshow-thumbs ul").children().removeClass("selected");
            $("#thumb_" + index).addClass("selected");
        },
        selectThumb: function (e) {
             var current_target = $(e.currentTarget)
                , current_id = current_target.attr("id");
            this.removeClass();
            current_target.addClass("selected");
            vent.trigger("clickedThumb", current_id);
        },
        removeClass: function() { 
            $(".slideshow-thumbs ul").children().removeClass("selected");
        }
    });

    Slideshow.Views.ThumbItemView = Backbone.View.extend({
        tagName: "li",
        className: "thumb_nail",
    });
    return Slideshow;

});
